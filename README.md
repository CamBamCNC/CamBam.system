# CamBam system folder

This area contains the files included in the default CamBam system folder installation.

The CamBam system folder is a location for...

- Configuration files
- Language translation files
- Drawing Templates
- Post Processors
- CAM Styles
- Tool Libraries
- Sample files
- Scripts
- Machine definitions
- Materials

Download from this repository to restore any configuration files to their default settings.
